# BoreholeResponseFunctions

This package contains a set of methods required to build borehole response functions. The package provide implementations for the Infinite Line Source model and for the short term response model by Claesson and Javed (2011).


## Example of usage
```
using BoreholeResponseFunctions
using Parameters

props = Para()
@unpack λg, Rb = props

st = 10 .^range(log10(10.), stop=log10(3600*1e5), length=200)
Tst = [gClaessonJaved(t,props)  for t in st]
gst   = [(2pi*λg)*(gClaessonJaved(t,props))  for t in st]  

Tils  = [1/(2pi*λg) * ILS(t, props) + Rb for t in st]
gils  = [ILS(t, props) + props.Rb * (2pi*λg ) for t in st]
```

The ILS model and Claesson/Javed models are equivalent for "long" time scales but shows a great difference in the short term.

<img src="./assets/comparison_gClaessonJaved_VS_ILS.png" width="600" height="400" />

The interface for the methods for the `ILS` method and for the `gClaessonJaved` method take as parameters the time t and an object params of type `Para`. Para contains all the parameters necessary to describe the configuration.
```
julia> Para()
Para{Float64}
  rb: Float64 0.0575
  λb: Float64 1.5
  Cb: Float64 3.1e6
  αb: Float64 4.838709677419355e-7
  λg: Float64 3.0
  Cg: Float64 1.875e6
  αg: Float64 1.6e-6
  rp: Float64 0.03959797974644667
  λp: Float64 0.42
  dpw: Float64 0.0023
  hp: Float64 725.0
  Cw: Float64 20600.604268426025
  Rgrout: Float64 0.039577255708385745
  Rp: Float64 0.028219207068815616
  Rb: Float64 0.06779646277720136
  ```

  
  