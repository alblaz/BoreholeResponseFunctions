module BoreholeResponseFunctions

# include("E1.jl") #necessary until expint will be included in SpecialFunctions.jl
include("defaults.jl")
include("quick_constructors.jl")

export ILS, gClaessonJaved, Para
export generate_gfunction_short_term
export seg2seg, seg2seg_numb
export ierf
# export integrand,L
# export expint

export mils_steady_state, intergrand_mils, mils, log_mils_steady_state_r_phi
export mfls, mfls_adiabatic_surface

####################################################################
#INFINITE LINE SOURCE
using QuadGK,SpecialFunctions, Roots
using FastGaussQuadrature

include("moving_sources.jl")

"""
ILS(Fo::AbstractFloat) 
ILS(t::T,r::T,α::T) where {T<:AbstractFloat} 
ILS(t::T,p::Para) where {T<:AbstractFloat}

Infinite Line source solution solution offers three methods. 
It can be called with a single argument and it return the ILS as a function of the fourier number Fo = α*t/r^2 where r [m] is the radius of the borehole, α [m^2/s^2] and t [s] the time at which the function is evaluated.
It can be called explicitily as a function of t, r and α.
Or it can be called as a function of t and p::Para where p has type Para and contains a  set of useful parameters to define the properties of a borehole system.
"""
ILS(Fo::AbstractFloat) = 1/2*expint(1/(4*Fo))
#dimensional expression of time t, thermal diffusivity α and radius r
ILS(t::T,r::T,α::T) where {T<:AbstractFloat} = ILS( (α*t)/r^2 )
#dimensional expression of time t, thermal diffusivity α and radius r
ILS(t::T,p::Para) where {T<:AbstractFloat}= ILS( (p.αg*t)/p.rb^2 )


####################################################################
# SHORT TERM BOREHOLE RESPONSE ACCORDING TO THE MODEL PROPOSED BY CLAESSON-JAVED
function L(u,props::Para,t0)

	@unpack αb, αg, rb, rp, λb, λg, Cw, Rp = props

	τp = rp/(sqrt(αb*t0))
	τb = rb/(sqrt(αb*t0))
	τg = rb/(sqrt(αg*t0))

	Jb0 = besselj0(τb*u);
	Jp0 = besselj0(τp*u);
	Yb0 = bessely0(τb*u);
	Yp0 = bessely0(τp*u);
	Jg0 = besselj0(τg*u);
	Yg0 = bessely0(τg*u);

	Jp1 = besselj1(τp*u);
	Yp1 = bessely1(τp*u);
	Jb1 = besselj1(τb*u);
	Yb1 = bessely1(τb*u)
	Jg1 = besselj1(τg*u);
	Yg1 = bessely1(τg*u);

	Kt = 4*λb/(Jp0*Yb0-Yp0*Jb0)
	Kb = 4 *λb*(0.5*pi*τb*u*(Jb1*Yp0-Yb1*Jp0) - 1)/(Jp0*Yb0-Yp0*Jb0)
	Kg = 2*pi*λg* (τg*u* (Jg1-1.0im*Yg1)) / (Jg0-1.0im*Yg0)
	Kp = 4 *λb*(0.5*pi*τp*u*(Jp1*Yb0-Yp1*Jb0) - 1)/(Jp0*Yb0-Yp0*Jb0)

	R1 = 1/Kt + 1/(Kb + Kg)
	R2 = Kp + 1/R1
	R3 = Rp + 1/R2
   	R4 = Cw*(-u^2/t0) + 1/R3
	return imag(-1/R4)
end

function integrand(u,t,props)
	t0 = 10. 	# reference time in seconds #I do not think that this does really matter
	return 2/pi* (1-exp(-u^2 * t/t0))/u * L(u,props,t0)
end

function integrand_seg2seg(u, r, δz, H)
	return 1/u^2*exp(-r^2*u^2)*(ierf((δz+H)*u)-2*ierf(δz*u)+ierf((δz-H)*u))
end

function ierf(x)
	return x*erf(x)-1/sqrt(pi)*(1-exp(-x^2))
end

"""
gClaessonJaved(t::T,props::Dict{Symbol,T}) where {T<:AbstractFloat}

t: time in seconds

props: property structure obtained using Parameters.jl. The structure should contains

	   rb => borehole radius
	   rp => equivalent pipe radius
	   λb => grout conductivity
	   αb => grout diffusivity
	   λg => ground conductivity
	   αg => ground diffusivity
	   Cw => water thermal capacity
	   Rp => pipe resistance

The function compute the borehole response according to the model proposed by Claesson and Javed for
a time t given the configuration properties described in props.

Javed, S, Claesson, J, 2011. New analytical and numerical solutions for the
short-term analysis of vertical ground heat exchangers. ASHRAE Transactions,
vol. 117(1): 3-12.
"""
function gClaessonJaved(t::T,props::Para)::T where {T<:AbstractFloat}
      return quadgk(u->integrand(u,t,props), 0., 30.)[1]
end

"""
seg2seg(t::T,props::Para, δr::T, δz::T,H::T)::T where {T<:AbstractFloat}
seg2seg(t::T, αg::T, δr::T, δz::T, H::T)::T where {T<:AbstractFloat}
seg2seg(t::Array{T,1}, props::Para, δr::T, δz::T, H::T)::Array{T,1}  where {T<:AbstractFloat}
seg2seg(t::Array{T,1}, αg::T, δr::T, δz::T, H::T)::Array{T,1} where {T<:AbstractFloat}
seg2seg_numb(t::T, props::Para, δr::T, δz::T, H::T)::T where {T<:AbstractFloat}
seg2seg_numb(t::T, αg::T, δr::T, δz::T, H::T)::T where {T<:AbstractFloat}


t: time in s (it can be a single value or an array)
δr: radial distance between the segments in m
δz: longitudinal distance between the segments in m
H: height of the segments in m (the segments are supposed to have the same height)
props: property structure obtained using Parameters.jl. The structure should contains

	   rb => borehole radius
	   rp => equivalent pipe radius
	   λb => grout conductivity
	   αb => grout diffusivity
	   λg => ground conductivity
	   αg => ground diffusivity
	   Cw => water thermal capacity
	   Rp => pipe resistance

αg: ground diffusivity in m2s-1


The function computes the thermal response factor on a borehole segment of height H
due to a step load on an equally long borehole segment at distance δr,δz (respectively
radial and axial distances). The segments are in an infinite medium. The result is not
scaled with the ground conductivity.

The function seg2seg_numb calculates the integral as given in the reference (upperboundary going to Inf).
The function seg2seg evaluates a reasonable upperboundary to use in the integration instead of Inf.
This leads to a decrease in the computational time.

Cimmino, M., Bernier, M., 2014. A semi-analytical method to generate g-functions
for geothermal bore fields. International Journal of Heat and Mass Transfer 70, 641–650.
"""
function seg2seg(t::T, props::Para, δr::T, δz::T, H::T)::T where {T<:AbstractFloat}
	f(u)=(1/u^2*exp(- δr^2*u^2)*(ierf((δz+H)*u)-2*ierf(δz*u)+ierf((δz-H)*u)))-1e-16;
	# tip= sqrt(6. / δr^2);   		#this number comes from exp(-(r*u)^2)=1e-16, term that makes the function decrease fast

	tip1 = sqrt(6. / (δr)	^2);
	tip2 = 6/(δz - H)
	tip = max(tip1,tip2)

	upb=tip
	# println(upb)
	upb=find_zero(f,tip, atol=1e-16);
	lb = 1/sqrt(4*props.αg*t)
	out = (upb > lb) ?  1/2/H * quadgk(u->integrand_seg2seg(u, δr, δz, H), lb, upb)[1] : 0
	return out
end

function seg2seg(t::T, αg::T, δr::T, δz::T, H::T)::T where {T<:AbstractFloat}
	f(u)=(1/u^2*exp(-δr^2*u^2)*(ierf((δz+H)*u)-2*ierf(δz*u)+ierf((δz-H)*u)))-1e-16;
	# tip= sqrt(6. / δr^2);
	tip1 = sqrt(6. / (δr)	^2);
	tip2 = 6. / (δz - H)
	tip = max(tip1,tip2)

	upb = find_zero(f,tip, atol=1e-16);
	lb = 1/sqrt(4*αg*t)
	out = (upb > lb) ?  1/2/H * quadgk(u->integrand_seg2seg(u, δr, δz, H), lb, upb)[1] : 0.
	return out
end

function seg2seg(t::Array{T,1}, props::Para, δr::T, δz::T, H::T)::Array{T,1}  where {T<:AbstractFloat}
	f(u)=(1/u^2*exp(-δr^2*u^2)*(ierf((δz+H)*u)-2*ierf(δz*u)+ierf((δz-H)*u)));
	
	# exp(-r^2*u^2)  ->  0 when r*u > 6  ->  u > r/6
	upb1 = 6. / δr ;
	
	# when δz - H < 0 second term (summation of ierf ...) is not equal to 0
	# but when δz - H > 0 it goes to 0 when u > 6/(δz - H) 
	# to show this it is necessary to look at all the terms within the sumation of ierf ...
	upb2 = (δz - H)>0 ? 6/(δz - H) : Inf  

	# we pick the minimum upper bound as our upperbound for the integration
	upb = min(upb1,upb2)

	lb = 1 ./ sqrt.(4 .* props.αg .* t);
	partial_integrals = fill(0.,length(t));	

	partial_integrals[1] = (upb > lb[1]) ?  1/2/H * p_adaptive_integration(u->integrand_seg2seg(u, δr, δz, H), lb[1], upb; n0 = 5,  atol = 1e-15) : 0.
	
	for ii in length(t):-1:2
		if (upb < lb[ii])
			break
		end
		partial_integrals[ii] =  1 / 2 / H * p_adaptive_integration(u->integrand_seg2seg(u, δr, δz, H), lb[ii], lb[ii-1]; n0 = 6,  atol = 1e-15) 
	end

	out=cumsum(partial_integrals)
end

function seg2seg(t::Array{T,1}, αg::T, δr::T, δz::T, H::T)::Array{T,1} where {T<:AbstractFloat}
	f(u)=(1/u^2*exp(-δr^2*u^2)*(ierf((δz+H)*u)-2*ierf(δz*u)+ierf((δz-H)*u)));
	
	# exp(-r^2*u^2)  ->  0 when r*u > 6  ->  u > r/6
	upb1 = 6. / δr ;
	
	# when δz - H < 0 second term (summation of ierf ...) is not equal to 0
	# but when δz - H > 0 it goes to 0 when u > 6/(δz - H) 
	# to show this it is necessary to look at all the terms within the sumation of ierf ...
	upb2 = (δz - H)>0 ? 6/(δz - H) : Inf  

	# we pick the minimum upper bound as our upperbound for the integration
	upb = min(upb1,upb2)

	lb = 1 ./ sqrt.(4 .* αg .* t);
	partial_integrals = fill(0.,length(t));	

	partial_integrals[1] = (upb > lb[1]) ?  1/2/H * p_adaptive_integration(u->integrand_seg2seg(u, δr, δz, H), lb[1], upb; n0 = 5,  atol = 1e-15) : 0.
	
	for ii in length(t):-1:2
		if (upb < lb[ii])
			break
		end
		partial_integrals[ii] =  1 / 2 / H * p_adaptive_integration(u->integrand_seg2seg(u, δr, δz, H), lb[ii], lb[ii-1]; n0 = 6,  atol = 1e-15) 
	end

	out=cumsum(partial_integrals)
	return out
end

function seg2seg_numb(t::T, props::Para, δr::T, δz::T, H::T)::T where {T<:AbstractFloat}
	return 1/2/H * quadgk(u->integrand_seg2seg(u, δr, δz, H), 1/sqrt(4*props.αg*t), Inf)[1]
end

function seg2seg_numb(t::T, αg::T, δr::T, δz::T, H::T)::T where {T<:AbstractFloat}
	return 1/2/H * quadgk(u->integrand_seg2seg(u, δr, δz, H), 1/sqrt(4*αg*t), Inf)[1]
end

################################################################################################
################################################################################################
function gaussintegration(f::Function, a::Float64, b::Float64, n::Int)                                
    z, w = gausslegendre(n)
    halfinterval = (b-a)/2
    midpoint = (b+a)/2        
    xGauss = z .* halfinterval .+ midpoint
    wGauss = w .* halfinterval
    return sum(f.(xGauss) .* wGauss)    
end


function p_adaptive_integration(f::Function, a::Float64, b::Float64; n0 = 4, atol = 1e-12)        
    n = n0
    yc = gaussintegration(f,a,b,n)

    for i =1:1000
        n +=1 
        yn = gaussintegration(f,a,b,n)
        ϵ = abs(yn - yc)
        yc = yn
        # println("epsilon = $ϵ")
        if ϵ < atol
            break
        end
	end 
	if n == n0 + 1000
		println("reached max degree n = $n")
	end

    # println("n = $n")
    return yc
end

end # module
