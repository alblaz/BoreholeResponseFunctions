using Dierckx

"""
generate_gfunction_short_term(props::Para)

Return a Spline1D element which takes as argument time in seconds and return the non-dimensional response function.
"""
function generate_gfunction_short_term(props::Para; tmin = 10., tmax = 3600*1e5)
    @unpack λg, Rb = props
    st = 10 .^range(log10(tmin), stop=log10(tmax), length=200)
    gst   = [(2pi*λg)*(gClaessonJaved(t,props))  for t in st]  #- Rgrout - Rp
    Dierckx.Spline1D(st,gst)   # Interpolation with cubic spline
end